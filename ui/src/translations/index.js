module.exports = {
  en_NZ: require('./en_NZ'),
  en_US: require('./en_US'),
  es_ES: require('./es_ES'),
  mi_NZ: require('./mi_NZ'),
  pt_BR: require('./pt_BR')
}
